package fixtitles

import "context"

// Store is an interface for persisting Subtitles and Displays.
type Store interface {
	GetSubtitle(context.Context, string) (Subtitle, error)
	CreateSubtitle(context.Context, Subtitle) error
	UpdateSubtitle(context.Context, string, SubtitleChange) error
	ListSubtitles(context.Context, SubtitleFilter) ([]Subtitle, error)

	CreateDisplay(context.Context, Display) error
	ListDisplays(context.Context, DisplayFilter) ([]Display, error)
}

// Subtitle represents a single image file that is meant to be written
// to the screen, and the metadata around it.
type Subtitle struct {
	ID            string
	ImageFile     string
	OCRText       string
	ConfirmedText string
	Displays      []Display
}

// Display represents a specific instance of a Subtitle being written
// to the screen.
type Display struct {
	Start      int64
	End        int64
	Forced     bool
	X          int
	Y          int
	Group      string
	SubtitleID string
}

// DisplayFilter represents a filter to select only some displays.
type DisplayFilter struct {
	Groups []string
}

// SubtitleChange represents a requested change to a Subtitle's
// properties.
type SubtitleChange struct {
	ConfirmedText *string
	OCRText       *string
}

// Apply creates a Subtitle that is a copy of the Subtitle it is
// called on, with the changes specified in `change` applied.
func (s Subtitle) Apply(change SubtitleChange) Subtitle {
	if change.ConfirmedText != nil {
		s.ConfirmedText = *change.ConfirmedText
	}
	if change.OCRText != nil {
		s.OCRText = *change.OCRText
	}
	return s
}

// SubtitleFilter represents a filter to select only some subtitles.
type SubtitleFilter struct {
	ImageFiles []string
	Confirmed  *bool
	HasOcrText *bool
	Limit *int
}

// Matches returns true if the passed Subtitle should be considered
// a match for the filter.
func (f SubtitleFilter) Matches(s Subtitle) bool {
	if f.Confirmed != nil {
		if *f.Confirmed == true && s.ConfirmedText == "" {
			return false
		}
		if *f.Confirmed == false && s.ConfirmedText != "" {
			return false
		}
	}
	if f.HasOcrText != nil {
		if *f.HasOcrText == true && s.OCRText == "" {
			return false
		}
		if *f.HasOcrText == false && s.OCRText != "" {
			return false
		}
	}
	return true
}
