package apiv1

import (
	"code.carvers.co/fixtitles"
)

type Service struct {
	SourcePath string
	Storage    fixtitles.Store
}
