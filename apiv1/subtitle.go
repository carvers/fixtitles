package apiv1

import (
	"net/http"

	"code.carvers.co/fixtitles"
	"darlinggo.co/trout"
)

type Subtitle struct {
	ImageFile     string    `json:"imageFile"`
	OCRText       string    `json:"ocrText"`
	ConfirmedText string    `json:"confirmedText"`
	Displays      []Display `json:"displays"`
}

type Display struct {
	Start  int64  `json:"start"`
	End    int64  `json:"end"`
	Forced bool   `json:"forced"`
	X      int    `json:"x"`
	Y      int    `json:"y"`
	Group  string `json:"group"`
}

type SubtitleChange struct {
	ConfirmedText *string `json:"confirmed_text"`
	OCRText       *string `json:"ocr_text"`
}

func coreSubtitle(sub Subtitle) fixtitles.Subtitle {
	return fixtitles.Subtitle{
		ImageFile:     sub.ImageFile,
		OCRText:       sub.OCRText,
		ConfirmedText: sub.ConfirmedText,
		Displays:      coreDisplays(sub.Displays),
	}
}

func apiSubtitle(sub fixtitles.Subtitle) Subtitle {
	return Subtitle{
		ImageFile:     sub.ImageFile,
		OCRText:       sub.OCRText,
		ConfirmedText: sub.ConfirmedText,
		Displays:      apiDisplays(sub.Displays),
	}
}

func apiSubtitleChange(sub fixtitles.SubtitleChange) SubtitleChange {
	return SubtitleChange{
		ConfirmedText: sub.ConfirmedText,
		OCRText:       sub.OCRText,
	}
}

func coreSubtitleChange(sub SubtitleChange) fixtitles.SubtitleChange {
	return fixtitles.SubtitleChange{
		ConfirmedText: sub.ConfirmedText,
		OCRText:       sub.OCRText,
	}
}

func apiSubtitles(subs []fixtitles.Subtitle) []Subtitle {
	res := make([]Subtitle, 0, len(subs))
	for _, s := range subs {
		res = append(res, apiSubtitle(s))
	}
	return res
}

func coreDisplay(disp Display) fixtitles.Display {
	return fixtitles.Display{
		Start:  disp.Start,
		End:    disp.End,
		Forced: disp.Forced,
		X:      disp.X,
		Y:      disp.Y,
		Group:  disp.Group,
	}
}

func apiDisplay(disp fixtitles.Display) Display {
	return Display{
		Start:  disp.Start,
		End:    disp.End,
		Forced: disp.Forced,
		X:      disp.X,
		Y:      disp.Y,
		Group:  disp.Group,
	}
}

func apiDisplays(disps []fixtitles.Display) []Display {
	res := make([]Display, 0, len(disps))
	for _, d := range disps {
		res = append(res, apiDisplay(d))
	}
	return res
}

func coreDisplays(disps []Display) []fixtitles.Display {
	res := make([]fixtitles.Display, 0, len(disps))
	for _, d := range disps {
		res = append(res, coreDisplay(d))
	}
	return res
}

func (s Service) Server(baseURL string) http.Handler {
	var router trout.Router
	router.SetPrefix(baseURL)

	router.Endpoint("/subtitle").Handler(http.HandlerFunc(s.handleSubtitlesList))
	router.Endpoint("/subtitle/{id}").Methods("PATCH").Handler(http.HandlerFunc(s.handleUpdateSubtitle))
	router.Endpoint("/subtitle/{id}/file").Handler(http.HandlerFunc(s.handleImage))

	return router
}
