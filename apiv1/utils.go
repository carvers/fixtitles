package apiv1

import (
	"encoding/json"
	"log"
	"net/http"
)

// RequestError describes an error with an HTTP request,
// indicating how it should be fixed.
type RequestError struct {
	Slug   string `json:"error,omitempty"`
	Field  string `json:"field,omitempty"`
	Param  string `json:"param,omitempty"`
	Header string `json:"header,omitempty"`
}

// Response represents an API response.
type Response struct {
	Subtitles []Subtitle     `json:"subtitles"`
	Errors    []RequestError `json:"errors,omitempty"`
	Code      int            `json:"-"`
}

type UpdateRequestBody struct {
	ConfirmedText string `json:"confirmed_text"`
}

// Encode writes a Response to the ResponseWriter with the given status.
func Encode(w http.ResponseWriter, r *http.Request, resp Response) {
	w.Header().Set("content-type", "application/json")
	status := resp.Code
	if status == 0 {
		status = http.StatusOK
	}
	w.WriteHeader(status)
	enc := json.NewEncoder(w)
	err := enc.Encode(resp)
	if err != nil {
		log.Println(err)
	}
}

// Decode reads the request body and parses it into `target`, using the
// appropriate decoder for the request's Content-Type header.
func Decode(r *http.Request, target interface{}) error {
	defer r.Body.Close()
	switch r.Header.Get("Content-Type") {
	case "application/json":
		dec := json.NewDecoder(r.Body)
		return dec.Decode(target)
	default:
		dec := json.NewDecoder(r.Body)
		return dec.Decode(target)
	}
}
