package apiv1

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"path"
	"strconv"
	"strings"

	"code.carvers.co/fixtitles"
	"darlinggo.co/trout"
)

func GetSubtitle(ctx context.Context, id string, s Service) (fixtitles.Subtitle, error) {
	return s.Storage.GetSubtitle(ctx, id)
}

func ListSubtitles(ctx context.Context, filter fixtitles.SubtitleFilter, s Service) ([]fixtitles.Subtitle, error) {
	return s.Storage.ListSubtitles(ctx, filter)
}

func UpdateSubtitle(ctx context.Context, imageFile string, change fixtitles.SubtitleChange, s Service) error {
	return s.Storage.UpdateSubtitle(ctx, imageFile, change)
}

func ParseSubtitleFilter(v url.Values) (fixtitles.SubtitleFilter, Response) {
	f := fixtitles.SubtitleFilter{}
	if confirmed := v.Get("confirmed"); confirmed != "" {
		switch strings.TrimSpace(strings.ToLower(confirmed)) {
		case "true":
			b := true
			f.Confirmed = &b
		case "false":
			b := false
			f.Confirmed = &b
		default:
			return f, Response{
				Errors: []RequestError{{Param: "confirmed", Slug: "invalid_value"}},
				Code:   http.StatusBadRequest,
			}
		}
	}
	if limit := v.Get("limit"); limit != "" {
		l, err := strconv.Atoi(limit)
		if err != nil {
			return f, Response{
				Errors: []RequestError{{Param: "limit", Slug: "invalid_format"}},
				Code:   http.StatusBadRequest,
			}
		}
		if l < 0 {
			return f, Response{
				Errors: []RequestError{{Param: "limit", Slug: "invalid_value"}},
				Code:   http.StatusBadRequest,
			}
		}
		f.Limit = &l
	}
	return f, Response{}
}

func (s Service) handleSubtitlesList(w http.ResponseWriter, r *http.Request) {
	f, resp := ParseSubtitleFilter(r.URL.Query())
	if resp.Code != 0 {
		Encode(w, r, resp)
		return
	}

	subtitles, err := ListSubtitles(r.Context(), f, s)
	if err != nil {
		Encode(w, r, Response{Errors: []RequestError{{Header: "Content-Type", Slug: "invalid_format"}}, Code: http.StatusBadRequest})
		return
	}

	w.Header().Set("X-Fixtitles-Count", strconv.Itoa(len(subtitles)))

	if f.Limit != nil {
		subtitles = subtitles[0:*f.Limit]
	}

	Encode(w, r, Response{Subtitles: apiSubtitles(subtitles)})
}

func (s Service) handleImage(w http.ResponseWriter, r *http.Request) {
	params := trout.RequestVars(r)
	id := params.Get("ID")
	if id == "" {
		Encode(w, r, Response{Errors: []RequestError{{Param: "id", Slug: "not_found"}}, Code: http.StatusBadRequest})
		return
	}
	subtitle, err := GetSubtitle(r.Context(), id, s)
	if err != nil {
		Encode(w, r, Response{Errors: []RequestError{{Header: "Content-Type", Slug: "invalid_format"}}, Code: http.StatusBadRequest})
		return
	}

	http.ServeFile(w, r, path.Join(s.SourcePath, subtitle.ImageFile))
}

func (s Service) handleUpdateSubtitle(w http.ResponseWriter, r *http.Request) {
	params := trout.RequestVars(r)
	id := params.Get("ID")
	if id == "" {
		Encode(w, r, Response{Errors: []RequestError{{Param: "id", Slug: "not_found"}}, Code: http.StatusBadRequest})
		return
	}
	var b SubtitleChange
	err := Decode(r, &b)
	if err != nil || b.ConfirmedText == nil || *b.ConfirmedText == "" {
		Encode(w, r, Response{Errors: []RequestError{{Header: "Content-Type", Slug: "invalid_format"}}, Code: http.StatusBadRequest})
		return
	}
	err = UpdateSubtitle(r.Context(), id, coreSubtitleChange(b), s)
	if err != nil {
		fmt.Println(err)
		fmt.Println(id)
		Encode(w, r, Response{Errors: []RequestError{{Header: "Content-Type", Slug: "invalid_format"}}, Code: http.StatusBadRequest})
		return
	}
	subtitle, err := GetSubtitle(r.Context(), id, s)
	if err != nil {
		Encode(w, r, Response{Errors: []RequestError{{Header: "Content-Type", Slug: "invalid_format"}}, Code: http.StatusBadRequest})
		return
	}
	Encode(w, r, Response{Subtitles: []Subtitle{apiSubtitle(subtitle)}})
}
