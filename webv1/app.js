const imagePreview = document.getElementById('preview');
const textarea = document.getElementById('text');
const confirmButton = document.getElementById('confirm');
const subtitlesRemainingText = document.getElementById('subtitles-remaining');

function render({ imageUrl, ocrText, id, subtitlesRemaining }) {
  imagePreview.src = imageUrl;
  textarea.value = ocrText;
  confirmButton.dataset.id = id;
  subtitlesRemainingText.textContent = `Subtitles remaining: ${subtitlesRemaining}`;
}

async function confirmSubtitle(id, confirmed_text) {
  const response = await fetch(`/api/v1/subtitle/${id}`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ confirmed_text })
  });
  const data = await response.json();
  return data;
}

async function getSubtitle() {
  const response = await fetch('/api/v1/subtitle?limit=1&confirmed=false');
  const data = await response.json();
  const subtitles = data['subtitles'];
  const subtitlesRemaining = response.headers.get("X-Fixtitles-Count");
  return { subtitles, subtitlesRemaining };
}

async function renderNewSubtitle() {
  const { subtitles, subtitlesRemaining } = await getSubtitle();
  const subtitle = subtitles[0];
  console.log({ subtitle });
  render({
    imageUrl: `/api/v1/subtitle/${subtitle.id}/file`,
    ocrText: subtitle.ocr_text.trim(),
    id: subtitle.id,
    subtitlesRemaining
  });
}

async function confirmCurrentSubtitle() {
  const confirmedText = textarea.value;
  const subtitleId = confirmButton.dataset.id;

  await confirmSubtitle(subtitleId, confirmedText);
  console.log('confirmed subtitle, rendering new subtitle');
  await renderNewSubtitle();
}

function addKeyboardShortcuts() {
  document.addEventListener('keydown', async (e) => {
    if (e.key == 's' && e.metaKey == true) {
      // Command-S
      e.preventDefault();
      await confirmCurrentSubtitle();
    }
    
    return true;
  });
}

(async () => {
  addKeyboardShortcuts();
  await renderNewSubtitle();
  
  confirmButton.addEventListener('click', confirmCurrentSubtitle);
})();