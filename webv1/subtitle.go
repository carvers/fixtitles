package webv1

//go:generate togo -pkg webv1 -name indexHtml -input index.html
//go:generate togo -pkg webv1 -name appJs -input app.js

import (
	"net/http"

	"code.carvers.co/fixtitles"
	"darlinggo.co/trout"
)

type Subtitle struct {
	ImageFile     string
	OCRText       string
	ConfirmedText string
	Displays      []Display
}

type Display struct {
	Start  int64
	End    int64
	Forced bool
	X      int
	Y      int
	Group  string
}

func coreSubtitle(sub Subtitle) fixtitles.Subtitle {
	return fixtitles.Subtitle{
		ImageFile:     sub.ImageFile,
		OCRText:       sub.OCRText,
		ConfirmedText: sub.ConfirmedText,
		Displays:      coreDisplays(sub.Displays),
	}
}

func webSubtitle(sub fixtitles.Subtitle) Subtitle {
	return Subtitle{
		ImageFile:     sub.ImageFile,
		OCRText:       sub.OCRText,
		ConfirmedText: sub.ConfirmedText,
		Displays:      webDisplays(sub.Displays),
	}
}

func coreDisplay(disp Display) fixtitles.Display {
	return fixtitles.Display{
		Start:  disp.Start,
		End:    disp.End,
		Forced: disp.Forced,
		X:      disp.X,
		Y:      disp.Y,
		Group:  disp.Group,
	}
}

func webDisplay(disp fixtitles.Display) Display {
	return Display{
		Start:  disp.Start,
		End:    disp.End,
		Forced: disp.Forced,
		X:      disp.X,
		Y:      disp.Y,
		Group:  disp.Group,
	}
}

func webDisplays(disps []fixtitles.Display) []Display {
	res := make([]Display, 0, len(disps))
	for _, d := range disps {
		res = append(res, webDisplay(d))
	}
	return res
}

func coreDisplays(disps []Display) []fixtitles.Display {
	res := make([]fixtitles.Display, 0, len(disps))
	for _, d := range disps {
		res = append(res, coreDisplay(d))
	}
	return res
}

func IndexHtmlRoute(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	w.Write(indexHtml)
}

func AppJsRoute(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/javascript")
	w.Write(appJs)
}

func Server(baseURL string) http.Handler {
	var router trout.Router
	router.SetPrefix(baseURL)

	router.Endpoint("/").Handler(http.HandlerFunc(IndexHtmlRoute))
	router.Endpoint("/index.html").Handler(http.HandlerFunc(IndexHtmlRoute))
	router.Endpoint("/app.js").Handler(http.HandlerFunc(AppJsRoute))

	return router
}
