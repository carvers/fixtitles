package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"code.carvers.co/fixtitles"
	"code.carvers.co/fixtitles/apiv1"
	"code.carvers.co/fixtitles/stores"
	"code.carvers.co/fixtitles/webv1"
)

func readSubtitleFromJSON(filepath string) (fixtitles.Subtitle, error) {
	jsonFile, err := os.Open(filepath)
	if err != nil {
		return fixtitles.Subtitle{}, err
	}
	defer jsonFile.Close()

	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return fixtitles.Subtitle{}, err
	}
	var result fixtitles.Subtitle
	json.Unmarshal([]byte(byteValue), &result)

	return result, nil
}

func crawlJSONFiles(ctx context.Context, db fixtitles.Store, path string) error {
	return filepath.Walk(path, func(filepath string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if ctx.Err() != nil {
			return ctx.Err()
		}

		if strings.HasSuffix(filepath, ".json") {
			s, err := readSubtitleFromJSON(filepath)
			if err != nil {
				return err
			}

			err = db.CreateSubtitle(ctx, s)
			if err != nil {
				return err
			}
		}

		return nil
	})
}

func main() {
	var subtitleFolder string
	flag.StringVar(&subtitleFolder, "input", "", "folder where displays and subtitle information is located")
	flag.Parse()

	if subtitleFolder == "" {
		log.Fatal("--input must be specified")
	}
	if _, err := os.Stat(subtitleFolder); err != nil {
		log.Fatalf("error opening input %q: %s", subtitleFolder, err)
	}

	db, err := stores.NewMemory(subtitleFolder)
	if err != nil {
		log.Fatal(err)
	}

	ctx := context.Background()

	err = crawlJSONFiles(ctx, db, subtitleFolder)
	if err != nil {
		log.Fatal(err)
	}

	service := apiv1.Service{
		Storage:    db,
		SourcePath: subtitleFolder,
	}

	http.Handle("/api/v1/", service.Server("/api/v1"))
	http.Handle("/", webv1.Server(""))
	fmt.Println("Now listening on localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
