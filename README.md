# Fixtitles

A web app for checking that OCR output is correct for the subtitles it's describing.

## Dependencies

### `togo`

Assets for the web interface are built using [`togo`](https://github.com/flazz/togo). Make sure that's in your PATH and run `go generate` in the `webv1` directory.