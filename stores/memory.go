package stores

import (
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"path"

	"code.carvers.co/fixtitles"
	memdb "github.com/hashicorp/go-memdb"
)

var (
	schema = &memdb.DBSchema{
		Tables: map[string]*memdb.TableSchema{
			"Subtitle": &memdb.TableSchema{
				Name: "Subtitle",
				Indexes: map[string]*memdb.IndexSchema{
					"id": &memdb.IndexSchema{
						Name:   "id",
						Unique: true,
						Indexer: &memdb.StringFieldIndex{
							Field: "Id",
						},
					},
				},
			},
			"Display": &memdb.TableSchema{
				Name: "Display",
				Indexes: map[string]*memdb.IndexSchema{
					"id": &memdb.IndexSchema{
						Name:   "id",
						Unique: true,
						Indexer: &memdb.StringFieldIndex{
							Field: "SubtitleID",
						},
					},
				},
			},
		},
	}
)

type Memory struct {
	db           *memdb.MemDB
	SourceFolder string
}

func WriteJson(v interface{}, filename string) error {
	j, err := json.MarshalIndent(v, "", "\t")
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filename, j, 0644)
	if err != nil {
		return err
	}

	return nil
}

func NewMemory(sourceFolder string) (*Memory, error) {
	db, err := memdb.NewMemDB(schema)
	if err != nil {
		return nil, err
	}
	return &Memory{
		db:           db,
		SourceFolder: sourceFolder,
	}, nil
}

func (m *Memory) CreateSubtitle(ctx context.Context, sub fixtitles.Subtitle) error {
	txn := m.db.Txn(true)
	defer txn.Abort()

	exists, err := txn.First("Subtitle", "id", sub.ID)
	if err != nil {
		return err
	}
	if exists != nil {
		return errors.New("Subtitle already exists")
	}

	err = txn.Insert("Subtitle", &sub)
	if err != nil {
		return err
	}
	txn.Commit()
	return nil
}

func (m *Memory) UpdateSubtitle(ctx context.Context, id string, change fixtitles.SubtitleChange) error {
	txn := m.db.Txn(true)
	defer txn.Abort()
	subtitle, err := txn.First("Subtitle", "id", id)
	if err != nil {
		return err
	}
	if subtitle == nil {
		return errors.New("subtitle not found")
	}
	updated := subtitle.(*fixtitles.Subtitle).Apply(change)
	err = txn.Insert("Subtitle", &updated)
	if err != nil {
		return err
	}
	txn.Commit()

	err = WriteJson(updated, path.Join(m.SourceFolder, updated.ID+".json"))
	if err != nil {
		return err
	}

	return nil
}

func (m *Memory) GetSubtitle(ctx context.Context, id string) (fixtitles.Subtitle, error) {
	txn := m.db.Txn(false)
	subtitle, err := txn.First("Subtitle", "id", id)
	if err != nil {
		return fixtitles.Subtitle{}, err
	}
	if subtitle == nil {
		return fixtitles.Subtitle{}, errors.New("subtitle not found")
	}

	return *subtitle.(*fixtitles.Subtitle), nil
}

func (m *Memory) ListSubtitles(ctx context.Context, filter fixtitles.SubtitleFilter) ([]fixtitles.Subtitle, error) {
	txn := m.db.Txn(false)
	var subtitles []fixtitles.Subtitle
	subtitleIter, err := txn.Get("Subtitle", "id")
	if err != nil {
		return nil, err
	}
	for {
		subtitle := subtitleIter.Next()
		if subtitle == nil {
			break
		}
		s := *subtitle.(*fixtitles.Subtitle)
		if filter.Matches(s) {
			subtitles = append(subtitles, s)
		}
	}
	return subtitles, nil
}

func (m *Memory) CreateDisplay(ctx context.Context, display fixtitles.Display) error {
	return errors.New("Not implemented yet")
}

func (m *Memory) ListDisplays(ctx context.Context, filter fixtitles.DisplayFilter) ([]fixtitles.Display, error) {
	txn := m.db.Txn(false)
	var displays []fixtitles.Display
	displayIter, err := txn.Get("Display", "id")
	if err != nil {
		return nil, err
	}
	for {
		display := displayIter.Next()
		if display == nil {
			break
		}
		displays = append(displays, *display.(*fixtitles.Display))
	}
	return displays, nil
}
